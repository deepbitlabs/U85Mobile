﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordImpact : MonoBehaviour 
{

    public Transform hitTransform;
    public float swordVelocity = 20f;
    public float massOfSword = 90f;

    void Awake()
    {
        GetComponent<Rigidbody>().isKinematic = false;
        if (!hitTransform)
        {

        }
    }

    void Update()
    {
        GetComponent<Rigidbody>().mass = massOfSword;
        GetComponent<Rigidbody>().useGravity = true;
        GetComponent<Rigidbody>().velocity = (hitTransform.position - transform.position).normalized * swordVelocity;
    }
}
