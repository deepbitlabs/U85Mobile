﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour 
{
	public float maxHealth = 100f;
	public float curHealth;

	bool isDead;

	void Start()
	{
		curHealth = maxHealth;	
	}

	void Update()
	{
		if (curHealth <= 0) 
		{
			if (!isDead) 
			{
				killPlayer ();
			}
		}
	}

	public void killPlayer()
	{
		Debug.Log ("Player has died!");
	}

	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.tag == "right_hand" || col.gameObject.tag == "left_hand") 
		{
			if (gameObject.tag == "Player") 
			{
				Debug.Log ("He hit me!");
				curHealth -= 1f;
			}
		}
	}
}
