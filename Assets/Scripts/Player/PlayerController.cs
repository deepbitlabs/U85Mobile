﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public PlayerHealth playerHealth;


	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.tag == "Enemy_Weapon") 
		{
			if (gameObject.tag == "Player") 
			{
                playerHealth.curHealth -= 10f;
			}
		}
	}
}
