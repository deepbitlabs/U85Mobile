﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotJoint : MonoBehaviour
{
    public Rigidbody rb;
    public float force = 5f;
    public Vector3 forcePositionOffset;

    public float speed;

    public float gizmoSize;

    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position + forcePositionOffset, gizmoSize);
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        Vector3 move = new Vector3();

        rb.AddForce(move * speed);
    }
}
