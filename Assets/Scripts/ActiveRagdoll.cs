﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveRagdoll : MonoBehaviour
{
    public HingeJoint hj;
    public Rigidbody rb;
    public Transform Obj;
    public bool Inverter;

    public float thrust;

    void Update()
    {
        JointSpring js = hj.spring;

        js.targetPosition = Obj.transform.localEulerAngles.x;

        if (js.targetPosition > 180)
            js.targetPosition = js.targetPosition - 360;

        js.targetPosition = Mathf.Clamp(js.targetPosition, hj.limits.min + 5, hj.limits.max - 5);

        if (Inverter)
            js.targetPosition = js.targetPosition * -1;

        hj.spring = js;
    }

    void FixedUpdate()
    {
        rb.AddForce(transform.forward * thrust, ForceMode.Impulse);
    }
}
