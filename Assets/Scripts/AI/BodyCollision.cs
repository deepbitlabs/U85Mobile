﻿using RootMotion.Dynamics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyCollision : MonoBehaviour
{

    public PuppetMaster puppetMaster;

    public EnemyHealth enemyHealth;

	public int scoreValue = 10;

	public float sparkTime;

		


    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Weapon")
        {
            if(gameObject.tag == "Head" || gameObject.tag == "Spine" || gameObject.tag == "Hips" )
            {
                
                Debug.Log(gameObject.tag + " Has been hit with" + col.gameObject.tag);

                // Will be adding health points to enemy
                enemyHealth.curHealth -= 10f;

                // Score Increase
                ScoreController.score += scoreValue;
            }
        }
    }
}
