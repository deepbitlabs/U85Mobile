﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.Dynamics;

public class EnemyHealth : MonoBehaviour
{
    public float maxHealth = 100f;
    public float curHealth;

    public PuppetMaster puppetMaster;

	public EnemyController enemy;

//	public Hackable hackable;
//	public ToRagdollOrNot ragdollConfig;

    bool isDead;



    void Start()
    {
        curHealth = maxHealth;
        //anim.SetBool ("IsHurt", false);

        //		hackable.enabled = false;
        //		ragdollConfig.enabled = false;

    }

    void Update()
	{
        if (curHealth <= 0)
        {
            DeadEnemy();
        }
    }

    public void DeadEnemy()
    {
        //enemyAI.killEnemy();
        puppetMaster.Kill();
        enemy.agent.Stop();

		// Enabling both scripts
//		hackable.enabled = true;
//		ragdollConfig.enabled = true;

        // Destroying entire gameobject
        Destroy(transform.parent.gameObject, 2f);
    }
}