﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyController : MonoBehaviour {

	// Enemy Target
	public GameObject target;

	Vector3 destination;

	public NavMeshAgent agent;

	//public SpawnController spawner;

	public Animator anim;

	public float attackDistance;

	AnimatorClipInfo[] m_AnimatorClipInfo;

	private Vector3 lastPosition;

	// Enemy animation state
	public enum EnemyState
	{
		Idle,
		Walking,
		Attacking,
		BeingHit,
		Hurt,
		Waiting,
		celebrate
	}

	public EnemyState state = EnemyState.Walking;

	private EnemyState currentState;


	void Start()
	{
		agent = GetComponent<NavMeshAgent>();

		destination = agent.destination;

		target = GameObject.FindGameObjectWithTag("Player");

		m_AnimatorClipInfo = anim.GetCurrentAnimatorClipInfo (0);

		Debug.Log ("Starting clip : " + m_AnimatorClipInfo [0].clip);

		lastPosition = transform.position;
	}


	void Update()
	{
		if (Vector3.Distance(destination, target.transform.position) > 3.0f)
		{
			destination = target.transform.position;
			agent.destination = destination;
			anim.SetTrigger ("Walking");
			//anim.SetTrigger ("IsHurt") = false;
			agent.stoppingDistance = 2.23f;
		}


		UpdateDestination();


		// Making sure the enemy gameobject is facing target
		FaceTarget();

	}

	public void CurrentState(EnemyState state)
	{
		switch (state) 
		{
		case EnemyState.Idle:
			break;
		case EnemyState.Walking:
			UpdateWalking ();
			break;
		case EnemyState.Attacking:
			UpdateAttacking ();
			break;
		case EnemyState.Waiting:
			break;
		case EnemyState.celebrate:
			UpdateCelebrate ();
			break;
		case EnemyState.Hurt:
			UpdateHurt ();
			break;
		}
	}

	private void TransitionToWalking()
	{
		UpdateWalking ();

        state = EnemyState.Walking;
	}

	private void UpdateWalking()
	{
		anim.SetTrigger ("Walk");
	}

	private void SetDestination()
	{
		TransitionToAttacking ();
	}

	private void TransitionToAttacking()
	{
		UpdateAttacking ();

        state = EnemyState.Attacking;
	}

	private void UpdateAttacking()
	{
		ExecuteAttack ();
	}

	private void TransitionToHurt()
	{
		state = EnemyState.Hurt;
	}

	public void UpdateHurt()
	{
		anim.SetTrigger ("IsHurt");
	}

	private void TransitionToCelebrate()
	{
		UpdateCelebrate ();
	}

	private void UpdateCelebrate()
	{
		// Will add functionality here later!
	}

	private bool IsInAttackRange (Transform target)
	{
		float distance = Vector3.Distance (transform.position, target.position);
		return distance < attackDistance;
	}

	public void UpdateDestination()
	{

		Vector3 destination = target.transform.rotation * target.transform.position;
		destination = destination.normalized * attackDistance;
	}

	public bool IsAtDestination()
	{
		bool atTarget = false;
		if (!agent.pathPending) 
		{
			if (agent.remainingDistance <= attackDistance) 
			{
				atTarget = true;
				ExecuteAttack ();
			}
		}
		return atTarget;
	}


	public void FaceTarget()
	{
		Vector3 targetPosition = new Vector3 (target.transform.position.x,
			this.transform.position.y,
			target.transform.position.z);
		this.transform.LookAt (targetPosition);

		IsAtDestination ();
	}

	public void ExecuteAttack()
	{
		string attack = PickAttack ();
		anim.SetBool ("attack", true);
		Debug.Log ("Now attacking the player!");
	}

	public string PickAttack()
	{
		int attackNumber = UnityEngine.Random.Range (0, 1);
		switch (attackNumber) 
		{
		case 0:
			return "Punch1";
		case 1:
			return "Punch1";
		default:
			return "Punch1";
		}
	}
}
