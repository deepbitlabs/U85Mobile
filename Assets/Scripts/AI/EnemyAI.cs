﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyAI : MonoBehaviour
{
    // Enemy Target
    public GameObject target;

    Vector3 destination;
    public NavMeshAgent agent;

    //public SpawnController spawner;

    public Animator anim;

	float AttackDistance = 2f;

	float rotationSpeed = 3f;

	AnimatorClipInfo[] m_AnimatorClipInfo;


    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        destination = agent.destination;

        target = GameObject.FindGameObjectWithTag("Player");

		m_AnimatorClipInfo = anim.GetCurrentAnimatorClipInfo (0);

		Debug.Log ("Starting clip : " + m_AnimatorClipInfo [0].clip);
    }

    void Update()
    {
        if (Vector3.Distance(destination, target.transform.position) > 1.0f)
        {
            destination = target.transform.position;
            agent.destination = destination;
        }

        UpdateDestination();


        // Making sure the enemy gameobject is facing target
        FaceTarget();


		if (IsInAttackRange (target.gameObject.transform)) 
		{
			RotateTowards (target.gameObject.transform);
		}
        
    }

	private bool IsInAttackRange (Transform target)
	{
		float distance = Vector3.Distance (transform.position, target.position);
		return distance < AttackDistance;
	}

	private void RotateTowards(Transform target)
	{
		Vector3 direction = (target.position - transform.position).normalized;
		Quaternion lookRotation = Quaternion.LookRotation (direction);
		transform.rotation = Quaternion.Slerp (transform.rotation, lookRotation, Time.deltaTime * rotationSpeed);
	}

    public void UpdateDestination()
    {
        agent.updatePosition = true;
        agent.updateRotation = true;
    }

    private IEnumerator FollowTarget(float range, Transform target)
    {
        Vector3 previousTargetPosition = new Vector3(float.PositiveInfinity, float.PositiveInfinity);
        while (Vector3.SqrMagnitude(transform.position - target.position) > 0.1f)
        {
            // did target move more than at least a minimum amount since last destination set?
            if (Vector3.SqrMagnitude(previousTargetPosition - target.position) > 0.1f)
            {
                agent.SetDestination(target.position);
                previousTargetPosition = target.position;
            }
            yield return new WaitForSeconds(0.1f);
        }
        yield return null;
    }

    public void FaceTarget()
    {
		

		if (Vector3.Distance (transform.position, target.transform.position) > AttackDistance) 
		{
			target.transform.LookAt (target.transform.position);
		} 
		else 
		{
			ExecuteAttack ();
		}
    }

	public void ExecuteAttack()
	{
		string attack = PickAttack ();
		anim.SetTrigger ("attack");
		Debug.Log ("Now attacking the player!");
	}

	public string PickAttack()
	{
		int attackNumber = UnityEngine.Random.Range (0, 3);
		switch (attackNumber) 
		{
		case 0:
			return "";
		case 1:
			return "";
		case 2:
			return "";
		default:
			return "";
		}
	}
}
