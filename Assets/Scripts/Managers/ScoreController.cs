﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour 
{
	public static int score;
	public Text text;

	void Awake()
	{
		text = GetComponent<Text> ();
		score = 0;
	}

	void Update()
	{
		text.text = " " + score;
	}

	
}
